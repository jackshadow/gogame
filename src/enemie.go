package main

import (
	"math/rand"
)

type Enemie struct {
	Object
	Path
}

func (enemie *Enemie) moveToPlayer() {
	if player, ok := players[1]; ok {
		enemie.getPath(enemie.PosX, enemie.PosY, player.PosX, player.PosY)
		defer enemie.clearAll()
		if enemie.isGoalNear() {

			stepX := rand.Intn(50)
			stepY := rand.Intn(50)
			if board.isFree(stepX, stepY) {
				board.set(stepX, stepY, 1)
				board.set(enemie.PosX, enemie.PosY, 0)
				enemie.PosX = stepX
				enemie.PosY = stepY
			}
		}
		if !enemie.isGoalNear() && len(enemie.path) > 0 {
			stepX, stepY := enemie.moveByPath()
			if board.isFree(stepX, stepY) {
				board.set(stepX, stepY, 1)
				board.set(enemie.PosX, enemie.PosY, 0)
				enemie.PosX = stepX
				enemie.PosY = stepY
			}

		}

	}
}

func (enemie *Enemie) isGoalNear() bool {
	X := enemie.PosX
	Y := enemie.PosY

	X_NEW := X - 1
	Y_NEW := Y - 1

	for i := 1; i < 10; i++ {
		if enemie.isGoal(X_NEW, Y_NEW) {
			return true
		}
		if i%3 == 0 {
			Y_NEW++
			X_NEW = X_NEW - 3
		}

		X_NEW++

	}
	return false
}
