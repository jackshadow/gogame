package main

import (
	"math/rand"
	"time"
	"sync"
	"log"
)

type Board struct {
	grid    map[int]map[int]int
	enemies []Enemie
	m       sync.Mutex
}

func (board *Board) spawner() {
	lastId := 1000
	for {   start := time.Now()

		x := rand.Intn(1000)
		y := rand.Intn(1000)

		if len(board.enemies) < 10 && board.isFree(x, y) {
			lastId++
			newEnemy := Enemie{Object{lastId, x, y, "enemie"}, Path{} }
			board.enemies = append(board.enemies, newEnemy)
			board.set(x, y, 1)
		}
		for id, value := range board.enemies {
			value.moveToPlayer()
			board.enemies[id] = value
			broadcast <- value.Object
		}
		elapsed := time.Since(start)
		log.Printf("Time took %s", elapsed)
		time.Sleep(100 * time.Millisecond)
	}

}
func (board *Board) isFree(PosX int, PosY int) bool {
	board.m.Lock()
	defer board.m.Unlock()
	if cell, ok := board.grid[PosX][PosY]; ok && cell == 0 {
		return true
	} else {
		return false
	}
}
func (board *Board) set(PosX int, PosY int, Value int) {
	board.m.Lock()
	defer board.m.Unlock()
	board.grid[PosX][PosY] = Value
}
func CreateBoard() Board {
	var board Board
	board.grid = make(map[int]map[int]int)
	for x := 0; x < 100; x++ {
		board.grid[x] = make(map[int]int)
		for y := 0; y < 100; y++ {
			board.grid[x][y] = 0
		}
	}
	return board
}
