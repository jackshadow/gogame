package main

type Object struct {
	Id   int `json:"id"`
	PosX int `json:"x"`
	PosY int `json:"y"`
	Type string `json:"type"`
}

func (o *Object) getPosX() int {
	return o.PosX
}

func (o *Object) getPosY() int {
	return o.PosY
}

func (o *Object) move(direction string) {
	old_x := o.PosX
	old_y := o.PosY
	switch direction {
	case "down":
		o.PosY++
		break
	case "up":
		o.PosY--
		break
	case "left":
		o.PosX--
		break
	case "right":
		o.PosX++
		break
	default:
		return
	}

	if board.isFree(o.PosX, o.PosY) {
		board.set(o.PosX,o.PosY,1)
		board.set(old_x,old_y,0)
	} else {
		o.PosX = old_x
		o.PosY = old_y
	}
}
