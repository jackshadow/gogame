package main

import (
	"math"
)

type Path struct {
	grid  map[int]map[int]int
	goalX int
	goalY int
	queue []QueueElement
	path  []pathForReconstruct
}

func (path *Path) getPath(PosX int, PosY int, goalX int, goalY int) []pathForReconstruct {
	path.goalX = goalX
	path.goalY = goalY
	path.grid = make(map[int]map[int]int)
	path.setBoard(PosX, PosY, 0)

	status, _, _ := path.getClosest(PosX, PosY)
	if status == false && len(path.queue) > 0 {
		for {
			if len(path.queue) == 0 {
				break
			}
			element := path.popFromQueue()

			status, x_new, y_new := path.getClosest(element.X, element.Y)
			if status == true {
				path.path = append(path.path, pathForReconstruct{x_new, y_new, path.grid[x_new][y_new]})
				path.reconstructPath(x_new, y_new)
				break
			}
		}
	}

	return path.path
}
func (path *Path) clearAll() {
	path.grid = make(map[int]map[int]int)
	path.path = path.path[:0]
	path.queue = path.queue[:0]

}
func (path *Path) getClosest(PosX int, PosY int) (bool, int, int) {

	var weigh = path.grid[PosX][PosY]
	moving := [][2]int{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}
	//moving := [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];//с диагональю
	for _, move := range moving {
		x_new := PosX + move[0]
		y_new := PosY + move[1]

		if x_new >= 0 && y_new >= 0 && board.isFree(x_new, y_new) && !path.isset(x_new, y_new) || path.isGoal(x_new, y_new) {
			path.setBoard(x_new, y_new, weigh+1)
			if path.isGoal(x_new, y_new) {
				return true, x_new, y_new
			}
			path.queue = append(path.queue, QueueElement{x_new, y_new, path.distanceToGoal(x_new, y_new)})
		}
	}

	return false, 0, 0
}
func (path *Path) distanceToGoal(PosX int, PosY int) float64 {
	return path.distance(path.goalX, path.goalY, PosX, PosY)
}
func (path *Path) distance(x1 int, y1 int, x2 int, y2 int) float64 {
	//return math.Abs(float64(x2-x1)) + math.Abs(float64(y2-y1))

	return math.Sqrt(math.Pow(float64(x2-x1), 2) + math.Pow(float64(y2-y1), 2))
}
func (path *Path) isGoal(PosX int, PosY int) bool {
	return PosX == path.goalX && PosY == path.goalY
}
func (path *Path) reconstructPath(PosX int, PosY int) bool {
	max_weight := path.grid[PosX][PosY] - 1
	moving := [][2]int{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}
	//$moving = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];//с диагональю
	min_distance := math.Pow(10, 7)
	x_min, y_min := 1000000, 1000000
	for _, move := range moving {
		{
			x_new := PosX + move[0]
			y_new := PosY + move[1]
			if max_weight == path.grid[x_new][y_new] && path.isset(x_new, y_new) &&  min_distance > path.distanceToGoal(x_new, y_new) {
				min_distance = path.distanceToGoal(x_new, y_new)
				x_min = x_new
				y_min = y_new
			}
		}
	}
	if path.grid[x_min][y_min] == 0 {
		return true
	} else {
		path.path = append(path.path, pathForReconstruct{x_min, y_min, max_weight})
		return path.reconstructPath(x_min, y_min)
	}
	return false
}
func (path *Path) popFromQueue() QueueElement {
	currentMin_key := 0
	currentMin := math.Pow(10, 7)
	for id, element := range path.queue {
		if element.dist <= currentMin {
			currentMin = element.dist
			currentMin_key = id
		}
	}
	element := path.queue[currentMin_key]
	path.queue = append(path.queue[:currentMin_key], path.queue[currentMin_key+1:]...)
	return element

}
func (path *Path) isset(PosX int, PosY int) bool {
	if _, ok := path.grid[PosX][PosY]; ok {
		return true
	}
	return false
}
func (path *Path) setBoard(PosX int, PosY int, Value int) {
	if _, ok := path.grid[PosX]; !ok {
		path.grid[PosX] = make(map[int]int)
	}
	path.grid[PosX][PosY] = Value
}
func (path *Path) moveByPath() (int, int) {

	i := len(path.path) - 1
	element := path.path[i]
	path.path = path.path[i:]
	return element.X, element.Y
}

type QueueElement struct {
	X    int
	Y    int
	dist float64
}
type pathForReconstruct struct {
	X      int
	Y      int
	weight int
}
